PCBNEW-LibModule-V1  2018-11-27 22:14:45
# encoding utf-8
Units mm
$INDEX
03540101ZXGY
$EndINDEX
$MODULE 03540101ZXGY
Po 0 0 0 15 5bfdc1d5 00000000 ~~
Li 03540101ZXGY
Cd 03540101ZXGY
Kw Undefined or Miscellaneous
Sc 0
At STD
AR 
Op 0 0 0
T0 0.000 -0 1.27 1.27 0 0.254 N V 21 N "U**"
T1 0.000 -0 1.27 1.27 0 0.254 N I 21 N "03540101ZXGY"
DS -17.8 -6.4 17.8 -6.4 0.2 24
DS 17.8 -6.4 17.8 6.4 0.2 24
DS 17.8 6.4 -17.8 6.4 0.2 24
DS -17.8 6.4 -17.8 -6.4 0.2 24
DS -19.925 -7.4 19.925 -7.4 0.1 24
DS 19.925 -7.4 19.925 7.4 0.1 24
DS 19.925 7.4 -19.925 7.4 0.1 24
DS -19.925 7.4 -19.925 -7.4 0.1 24
DS -17.8 -2.54 -17.78 -6.4 0.1 21
DS -17.78 -6.4 17.78 -6.4 0.1 21
DS 17.78 -6.4 17.8 -2.54 0.1 21
DS -17.8 2.54 -17.8 6.4 0.1 21
DS -17.8 6.4 17.78 6.4 0.1 21
DS 17.78 6.4 17.8 2.54 0.1 21
$PAD
Po -17.800 -0
Sh "1" C 2.250 2.250 0 0 900
Dr 1.5 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 17.800 -0
Sh "2" C 2.250 2.250 0 0 900
Dr 1.5 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE 03540101ZXGY
$EndLIBRARY
