PCBNEW-LibModule-V1  2018-10-07 10:22:07
# encoding utf-8
Units mm
$INDEX
SOIC127P1030X265-18N
$EndINDEX
$MODULE SOIC127P1030X265-18N
Po 0 0 0 15 5bb9d03f 00000000 ~~
Li SOIC127P1030X265-18N
Cd DW (R-PDSO-G18)
Kw Transformer
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "T**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "SOIC127P1030X265-18N"
DS -5.95 -6.125 5.95 -6.125 0.05 24
DS 5.95 -6.125 5.95 6.125 0.05 24
DS 5.95 6.125 -5.95 6.125 0.05 24
DS -5.95 6.125 -5.95 -6.125 0.05 24
DS -3.75 -5.775 3.75 -5.775 0.1 24
DS 3.75 -5.775 3.75 5.775 0.1 24
DS 3.75 5.775 -3.75 5.775 0.1 24
DS -3.75 5.775 -3.75 -5.775 0.1 24
DS -3.75 -4.505 -2.48 -5.775 0.1 24
DS -3.4 -5.775 3.4 -5.775 0.2 21
DS 3.4 -5.775 3.4 5.775 0.2 21
DS 3.4 5.775 -3.4 5.775 0.2 21
DS -3.4 5.775 -3.4 -5.775 0.2 21
DS -5.7 -5.755 -3.75 -5.755 0.2 21
$PAD
Po -4.725 -5.08
Sh "1" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 -3.81
Sh "2" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 -2.54
Sh "3" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 -1.27
Sh "4" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 0
Sh "5" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 1.27
Sh "6" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 2.54
Sh "7" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 3.81
Sh "8" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -4.725 5.08
Sh "9" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 5.08
Sh "10" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 3.81
Sh "11" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 2.54
Sh "12" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 1.27
Sh "13" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 0
Sh "14" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 -1.27
Sh "15" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 -2.54
Sh "16" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 -3.81
Sh "17" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.725 -5.08
Sh "18" R 0.65 1.95 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE SOIC127P1030X265-18N
$EndLIBRARY
