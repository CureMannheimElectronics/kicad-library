PCBNEW-LibModule-V1  2018-11-19 21:56:25
# encoding utf-8
Units mm
$INDEX
11065072
$EndINDEX
$MODULE 11065072
Po 0 0 0 15 5bf33189 00000000 ~~
Li 11065072
Cd 1-106507-2-2
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 -16.560 8.49 1.27 1.27 0 0.254 N V 21 N "J**"
T1 -16.560 8.49 1.27 1.27 0 0.254 N I 21 N "11065072"
DS -43.11 -2.16 9.99 -2.16 0.2 24
DS 9.99 -2.16 9.99 12.84 0.2 24
DS 9.99 12.84 -43.11 12.84 0.2 24
DS -43.11 12.84 -43.11 -2.16 0.2 24
DS -43.11 -2.16 9.99 -2.16 0.1 21
DS 9.99 -2.16 9.99 12.84 0.1 21
DS 9.99 12.84 -43.11 12.84 0.1 21
DS -43.11 12.84 -43.11 -2.16 0.1 21
DS -44.11 -3.16 10.99 -3.16 0.1 24
DS 10.99 -3.16 10.99 20.14 0.1 24
DS 10.99 20.14 -44.11 20.14 0.1 24
DS -44.11 20.14 -44.11 -3.16 0.1 24
DS -35.76 12.84 -35.76 19.14 0.2 24
DS -35.76 19.14 2.64 19.14 0.2 24
DS 2.64 19.14 2.64 12.84 0.2 24
$PAD
Po 0.000 -0
Sh "1" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -2.760 -0
Sh "2" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -5.520 -0
Sh "3" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -8.280 -0
Sh "4" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -11.040 -0
Sh "5" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -13.800 -0
Sh "6" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -16.560 -0
Sh "7" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -19.320 -0
Sh "8" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -22.080 -0
Sh "9" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -24.840 -0
Sh "10" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -27.600 -0
Sh "11" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -30.360 -0
Sh "12" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -33.120 -0
Sh "13" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -1.380 2.54
Sh "14" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -4.140 2.54
Sh "15" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.900 2.54
Sh "16" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -9.660 2.54
Sh "17" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -12.420 2.54
Sh "18" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -15.180 2.54
Sh "19" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -17.940 2.54
Sh "20" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -20.700 2.54
Sh "21" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -23.460 2.54
Sh "22" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -26.220 2.54
Sh "23" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -28.980 2.54
Sh "24" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -31.740 2.54
Sh "25" C 1.300 1.300 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -40.060 1.27
Sh "MH1" C 4.800 4.800 0 0 900
Dr 3.2 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 6.940 1.27
Sh "MH2" C 4.800 4.800 0 0 900
Dr 3.2 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE 11065072
$EndLIBRARY
